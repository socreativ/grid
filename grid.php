<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';


?>


<?php if(have_rows('content')): ?>
<section id="<?= $id ?>" class="grid-block <?= $css ?>">
    <div class="grid">
        <?php while (have_rows('content')): the_row(); ?>

            <?php
                $link = get_sub_field('link');
                $icon = get_sub_field('icon');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                ?>
    
            <a class="grid__item" id="grid__item-<?= get_row_index(); ?>" href="<?= esc_url($link['url']); ?>" target="<?= esc_attr($link['target']); ?>">
                <?php if($icon): ?>
                    <img src="<?= $icon['url']; ?>" alt="<?= $icon['alt'] ?>">
                <?php endif; ?>
                <?php if ($title): ?>
                    <span class="grid__title"><?= $title ?></span>
                <?php endif; ?>
                <?php if($text): ?>
                    <p><?= $text ?></p>
                <?php endif; ?>
            </a>

        <?php endwhile; ?>
    </div>
</section>
<?php endif ;?>

